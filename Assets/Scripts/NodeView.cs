﻿
using System.Collections.Generic;
using UnityEngine;


public class NodeView : MonoBehaviour
{

    public GameObject tile;
    public GameObject arrow;

    private Node mNode;


    [Range(0, 0.5f)]
    public float borderSize = 0.15f;


    public void Initialize(Node node)
    {
        if ( tile != null )
        {
            gameObject.name = "Node (" + node.xIndex + ", " + node.yIndex + ")";
            transform.position = node.position;
            tile.transform.localScale = new Vector3(1f - borderSize, 1f, 1f - borderSize);
            mNode = node;
            EnableObject(arrow, false);
        }
    }


    public void SetTileColor(GameObject tile, Color newColor)
    {
        if ( tile != null )
        {
            Renderer renderer = tile.GetComponent<Renderer>();
            if ( renderer != null )
            {
                renderer.material.color = newColor;
            }
        }
    }


    public void SetTileColor(Color newColor)
    {
        SetTileColor(tile, newColor);
    }


    public void EnableObject(in GameObject gameObject, bool state)
    {
        if ( gameObject != null )
        {
            gameObject.SetActive(state);
        }
    }


    public void ShowArrow()
    {
        if ( mNode != null && arrow != null && mNode.previousNode != null )
        {
            EnableObject(arrow, true);

            Vector3 directionToPreviousNode = (mNode.previousNode.position - mNode.position).normalized;
            arrow.transform.rotation = Quaternion.LookRotation(directionToPreviousNode);
        }
    }

}
