﻿
using System.Collections.Generic;
using UnityEngine;


public class Graph : MonoBehaviour
{
    public Node[,] nodes;
    public List<Node> walls = new List<Node>();

    private int[,] mMapData;
    private int mWidth;
    public int Width { get { return mWidth; } }
    private int mHeight;
    public int Height { get { return mHeight; } }

    public static readonly Vector2[] allDirections =
    {
        new Vector2(0f, 1f),
        new Vector2(1f, 1f),
        new Vector2(1f, 0f),
        new Vector2(1f, -1f),
        new Vector2(0f, -1f),
        new Vector2(-1f, -1f),
        new Vector2(-1f, 0f),
        new Vector2(-1f, 1f)
    };


    public void Initialize(int[,] mapData)
    {
        mMapData = mapData;
        mWidth = mapData.GetLength(0);
        mHeight = mapData.GetLength(1);

        nodes = new Node[mWidth, mHeight];

        for ( int y = 0; y < mHeight; y++ )
        {
            for ( int x = 0; x < mWidth; x++ )
            {
                NodeType nodeType = (NodeType)mapData[x, y];
                Node newNode = new Node(x, y, nodeType)
                {
                    position = new Vector3(x, 0, y)
                };

                nodes[x, y] = newNode;

                if ( nodeType == NodeType.Blocked )
                {
                    walls.Add(newNode);
                }
            }
        }

        for ( int y = 0; y < mHeight; y++ )
        {
            for ( int x = 0; x < mWidth; x++ )
            {
                if ( nodes[x, y].nodeType != NodeType.Blocked )
                {
                    nodes[x, y].neighborNodes = GetNeighborNodes(x, y);
                }
            }
        }
    }


    public bool IsWithInBounds(int x, int y)
    {
        return (x >= 0 && x < mWidth && y >= 0 && y < mHeight);
    }


    List<Node> GetNeighborNodes(int x, int y, Node[,] nodeArray, Vector2[] directions)
    {
        List<Node> neighborNodes = new List<Node>();

        foreach ( Vector2 direction in directions )
        {
            int newX = x + (int)direction.x;
            int newY = y + (int)direction.y;

            if ( IsWithInBounds(newX, newY) && nodeArray[newX, newY] != null && nodeArray[newX, newY].nodeType != NodeType.Blocked )
            {
                neighborNodes.Add(nodeArray[newX, newY]);
            }
        }

        return neighborNodes;
    }


    List<Node> GetNeighborNodes(int x, int y)
    {
        return GetNeighborNodes(x, y, nodes, allDirections);
    }
}
