﻿
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


public class Pathfinder : MonoBehaviour
{

    private Node mStartNode;
    private Node mGoalNode;
    private Graph mGraph;
    private GraphView mGraphView;

    private Queue<Node> mFrontierNodes;
    private List<Node> mExploredNodes;
    private List<Node> mPathNodes;

    public Color startColor = Color.green;
    public Color goalColor = Color.red;
    public Color frontierColor = Color.magenta;
    public Color exploredColor = Color.gray;
    public Color pathColor = Color.cyan;

    public bool IsComplete = false;
    private int mIterations = 0;


    public void Initialize(in Graph graph, in GraphView graphView, in Node startNode, in Node goalNode)
    {
        Debug.Assert(startNode != null && goalNode != null && graph != null && graphView != null);
        Debug.Assert(startNode.nodeType != NodeType.Blocked && goalNode.nodeType != NodeType.Blocked);

        mGraph = graph;
        mGraphView = graphView;
        mStartNode = startNode;
        mGoalNode = goalNode;
        ShowColors(graphView, startNode, goalNode);

        mFrontierNodes = new Queue<Node>();
        mFrontierNodes.Enqueue(startNode);
        mExploredNodes = new List<Node>();
        mPathNodes = new List<Node>();

        for ( int x = 0; x < mGraph.Width; x++ )
        {
            for ( int y = 0; y < mGraph.Height; y++ )
            {
                mGraph.nodes[x, y].Reset();
            }
        }

        IsComplete = false;
        mIterations = 0;
    }


    public IEnumerator SearchRoutine(float timeStep = 0.1f)
    {
        yield return null;

        while ( IsComplete == false )
        {
            if ( mFrontierNodes.Count > 0 )
            {
                Node currentNode = mFrontierNodes.Dequeue();
                mIterations++;

                if ( mExploredNodes.Contains(currentNode) == false )
                {
                    mExploredNodes.Add(currentNode);
                }

                ExpandFrontier(currentNode);
                ShowColors();
                if ( mGraphView )
                {
                    mGraphView.ShowNodeArrows(mFrontierNodes.ToList());
                }
                
                yield return new WaitForSeconds(timeStep);
            }
            else
            {
                IsComplete = true;
            }
        }
    }


    private void ExpandFrontier(in Node node)
    {
        if ( node != null )
        {
            for ( int i = 0; i < node.neighborNodes.Count; i++ )
            {
                if ( mExploredNodes.Contains(node.neighborNodes[i]) == false &&
                     mFrontierNodes.Contains(node.neighborNodes[i]) == false )
                {
                    node.neighborNodes[i].previousNode = node;
                    mFrontierNodes.Enqueue(node.neighborNodes[i]);
                }
            }
        }
    }


    private void ShowColors(in GraphView graphView, in Node startNode, in Node goalNode)
    {
        Debug.Assert(graphView != null && startNode != null && goalNode != null);

        if ( mFrontierNodes != null )
        {
            graphView.SetNodeViewsColor(mFrontierNodes.ToList(), frontierColor);
        }

        if ( mExploredNodes != null )
        {
            graphView.SetNodeViewsColor(mExploredNodes, exploredColor);
        }

        NodeView startNodeView = graphView.nodeViews[startNode.xIndex, startNode.yIndex];
        startNodeView.SetTileColor(startColor);

        NodeView goalNodeView = graphView.nodeViews[goalNode.xIndex, goalNode.yIndex];
        goalNodeView.SetTileColor(goalColor);
    }


    private void ShowColors()
    {
        ShowColors(mGraphView, mStartNode, mGoalNode);
    }

}
