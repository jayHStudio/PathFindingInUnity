﻿
using System.Collections.Generic;
using UnityEngine;


public class DemoController : MonoBehaviour
{

    public MapData mapData;
    public Graph graph;

    public Pathfinder pathfinder;
    public int startX = 0;
    public int startY = 0;
    public int goalX = 15;
    public int goalY = 1;

    public float timeStep = 0.1f;


    private void Start()
    {
        if ( mapData != null && graph != null )
        {
            int[,] mapInstance = mapData.MakeMap();
            graph.Initialize(mapInstance);

            GraphView graphView = graph.GetComponent<GraphView>();

            if ( graphView != null )
            {
                graphView.Initialize(graph);
            }

            if ( graph.IsWithInBounds(startX, startY) && graph.IsWithInBounds(goalX, goalY) && pathfinder != null )
            {
                Node startNode = graph.nodes[startX, startY];
                Node goalNode = graph.nodes[goalX, goalY];

                pathfinder.Initialize(graph, graphView, startNode, goalNode);
                StartCoroutine(pathfinder.SearchRoutine(timeStep));
            }
        }
    }

}
