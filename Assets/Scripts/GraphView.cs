﻿
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(Graph))]
public class GraphView : MonoBehaviour
{

    public GameObject nodeViewPrefab;
    public NodeView[,] nodeViews;

    public Color baseColor = Color.white;
    public Color wallColor = Color.black;


    public void Initialize(Graph graph)
    {
        if ( graph == null )
        {
            Debug.LogWarning("GRAPHVIEW No graph to initialize!");
            return;
        }

        nodeViews = new NodeView[graph.Width, graph.Height];

        foreach ( Node eachNode in graph.nodes )
        {
            GameObject newInstance = Instantiate(nodeViewPrefab, Vector3.zero, Quaternion.identity);
            NodeView nodeView = newInstance.GetComponent<NodeView>();

            if ( nodeView != null )
            {
                nodeView.Initialize(eachNode);
                nodeViews[eachNode.xIndex, eachNode.yIndex] = nodeView;

                if ( eachNode.nodeType == NodeType.Blocked )
                {
                    nodeView.SetTileColor(wallColor);
                }
                else
                {
                    nodeView.SetTileColor(baseColor);
                }
            }
        }
    }


    public void SetNodeViewsColor(in List<Node> nodes, Color newColor)
    {
        foreach ( Node eachNode in nodes )
        {
            if ( eachNode != null )
            {
                NodeView nodeView = nodeViews[eachNode.xIndex, eachNode.yIndex];

                if ( nodeView != null )
                {
                    nodeView.SetTileColor(newColor);
                }
            }
        }
    }


    public void ShowNodeArrows(Node node)
    {
        if ( node != null )
        {
            NodeView nodeView = nodeViews[node.xIndex, node.yIndex];
            if ( nodeView != null )
            {
                nodeView.ShowArrow();
            }
        }
    }


    public void ShowNodeArrows(List<Node> nodes)
    {
        foreach ( Node eachNode in nodes )
        {
            ShowNodeArrows(eachNode);
        }
    }

}
